import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
/**
 *
 * @author Davis
 */
public class CalculadoraDavisTest {
    
    public CalculadoraDavisTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }
    /****************************************************/
    CalculadoraDavis objeto = new CalculadoraDavis();
    /****************************************************/
    @Test
    public void testSuma() {
        int resultado = objeto.suma(7,3);
        int esperado = 10;
        assertEquals(resultado,esperado);
    }
    /****************************************************/
    @Test
    public void testResta() {
        int resultado = objeto.resta(5,1);
        int esperado = 4;
        assertEquals(resultado,esperado);
    }
    /****************************************************/
    @Test
    public void testMultiplicacion() {
        int resultado = objeto.multiplicacion(7,7);
        int esperado = 49;
        assertEquals(resultado,esperado);
    }
    /****************************************************/
    @Test
    public void testDivision() {
        int resultado = objeto.division(25,5);
        int esperado = 5;
        assertEquals(resultado,esperado);
    }
    /****************************************************/
    @Test
    public void testEsNumero() {
        boolean resultado = objeto.EsNumero("7");
        boolean esperado = true;
        assertEquals(resultado,esperado);
    }
    /****************************************************/
}
